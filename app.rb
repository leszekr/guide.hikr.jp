require 'sinatra'
require 'jekyll'
require 'mongo'
require 'json/ext' # required for .to_json
require 'bson'

include Mongo

module Hikr
  class API < Sinatra::Base

    configure do
      conn = MongoClient.new("localhost", 27017)
      set :mongo_connection, conn
      set :mongo_db, conn.db('test')
    end

    get '/collections/?' do
      settings.mongo_db.collection_names.to_a.to_json
    end

    # list all documents in the test collection
    get '/documents/?' do
      content_type :json
      settings.mongo_db['test'].find.to_a.to_json
    end

    # find a document by its ID
    get '/document/:id/?' do
      content_type :json
      document_by_id(params[:id]).to_json
    end

    # insert a new document from the request parameters,
    # then return the full document
    post '/new_document/?' do
      content_type :json
      new_id = settings.mongo_db['test'].insert params
      document_by_id(new_id).to_json
    end



    helpers do
      # a helper method to turn a string ID
      # representation into a BSON::ObjectId
      #def object_id val
      #  BSON::ObjectId.from_string(val)
      #end

      def document_by_id id
        id = object_id(id) if String === id
        settings.mongo_db['test'].
          find_one(:_id => id).to_json
      end
    end

    set :public_folder, '_site'

    get '/foo' do
      options = Jekyll.configuration({})
      Jekyll::Commands::Build.process(options)
      'bar'
    end

    get '/edit' do
      'hello'
    end

    get '/save/*' do
      "param: #{params[:splat]}"
    end

    get '/*' do
        file_name = "./_site#{request.path_info}".gsub(%r{\/+},'/')
        if File.directory?(file_name)
          file_name = file_name+'/index.html'
        end
        if File.exists?(file_name)
          File.read(file_name)
        else
          raise Sinatra::NotFound
        end
    end

  end
end
