---
layout: course
title: Takao-san
center: [139.2158317565918, 35.645788484330524]
tags: [ .draft ]
map: 
 - /data/c/takao.json
features:
 - /mt/takao.html
 - /mt/kagenobu.html
author: hikr.jp
area: Hachioji
prefecture: Tokyo
license: CC-BY-NC
info:
---
