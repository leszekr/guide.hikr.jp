---
layout: course
title: Oume Hiking Course
header_image: http://www.flickr.com/photos/jambodave/11637158694
license: BY-CC-NC
area: Oume
author: David
prefecture: Tokyo
tags: [shrine, .draft]
center: [ 139.25848960876465, 35.79052583846516]
summary: >
  This course is a nice, gentle mountain hike in Tokyo.
---


## Points of interest

 - Tensou Shrine (天祖神社) with its long, steep stairway
 - Tengu Rock (天狗岩) with a wonderful view of the river and nearby city


## Access

Oume Station (青梅駅) is on the Oume Line (梅線) about 1 hour 10 minutes from Shinjuku, taking the Chuo Line to Tachikawa and changing there to the Oume Line, though there are a few of the Chuo Line trains from Shinjuku which go all the way to Oume.

## Course

{% flickr http://www.flickr.com/photos/jambodave/11637537766/in/set-72157639176278964 large half %}

From Oume Station, go out the only ticket gate and walk to the main road in front of you. Turn left there and walk about 10 minutes (to the 3rd road on the right past the post office). Turn right and stay on this main road (Rt 31) which veers to the left and then to the right before crossing the Tama River. After crossing the river and Rt 411 (a big street), keep an eye out for Tensou Shrine (天祖神社) on your right. There’s Torii gate and steep stairway there. Go up the stairs to the shrine.


Behind the shrine, you can see the sign for the hiking course marked KyuuNitsuka Pass (旧二っ塚峠). You’ll reach the pass in about 1 hour, then follow the sign to 馬引沢峠 and Tengu Rock (天狗岩).

{% flickr http://www.flickr.com/photos/jambodave/11637149674/in/set-72157639176278964 %}

In about 40 minutes you’ll come to a sign that points to Tengu Rock on the right and the main course on the right. DEFINITELY go to Tengu Rock (it has a spectacuar view of the town and the river from rock that juts out above the valley) and return to walk on the main path. Follow the signs to Miya-no-Hira Station (宮ノ平駅) for about 1.5 hours. You’ll come to a small road where you turn right, then right again at the main road. Keep to the left, cross the river and, at the lights, turn right on the main road which will take you near the station.

{%flickr http://www.flickr.com/photos/jambodave/11636776245/in/set-72157639176278964 %}

