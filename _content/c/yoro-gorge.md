---
layout: course
title: Umegase Gorge
author: David
prefecture: Chiba
area: Boso
license: BY-CC-NC
author: David
center: [140.1595401763916, 35.26650521823752]
map:
tags: [cliff, maples, .draft]
header_image: http://www.flickr.com/photos/jambodave/11741543205/
header_class: vertical
summary: >
  Umegase Gorge (梅が瀬渓谷) is on the Boso Peninsula in Chiba Prefecture. It is the most famous area in Chiba to see the autumn colors (especially the Japanese maples), so visiting at the end of November or beginning of December is best.
---

## Points of interest
 - Autumn colors (Japanese maples)
 - Mt. Daifuku Observation Tower

## Access

Yoro Keikoku station (養老渓谷駅) is on the Kominato Line (小湊鉄道) about 1 hour from Goi (五井). From Chiba Station, take the Uchibo Line to Goi (about 20 minutes), then change to the Kominato Line (which runs only about once every 2 hours).

## Course

From Yoro Keikoku Station’s only exit, turn right and walk along the tracks, on your right. Cross the tracks at the first crossing and turn right (so the tracks are on your right again). At the second street, turn left. You’ll walk across a high bridge (over a river) and continue straight until the road ends. Turn right there and walk past a pond and some cliffs. After passing a parking lot, you’ll come to a branch in the road. Keep to the left. The road on the right goes to Mt. Daifuku (大福山) and you will be returning down that road at the end of the hike and then follow the same way back to the station from there. Walking down the road on the left, you’ll start entering the gorge, with periodic cliffs and crossing a creek sometimes.

After about 1.5 hours, you’ll see a picnic table where you can have lunch. Nearby, you see a sign for Mt. Daifuku. Take this path (it’s an uphill climb with beautiful trees for about 30-40 minutes) and it ends at a road. At the road, turn right and walk down a couple of minutes, where there are some toilets and the Mt. Daifuku Observation tower.

After check that out, continue down the road for about an hour and it will run into a road (the one that branched earlier in the hike, where you took the left fork). Turn left there and follow your way back to the station, the same way you came.

This area also has Yoro Gorge (for which the station is named) and Yoro Falls if you want to see that on another day.

{% flickr http://www.flickr.com/photos/jambodave/11150624503 %}

