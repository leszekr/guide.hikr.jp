---
title: Tanzawa Waterfall Course
layout: course
tags: [waterfall, cedar]
header_image: http://www.flickr.com/photos/jambodave/11150562144
header_class: vertical
license: BY-CC-NC
author: David
center: [ 139.18407440185547, 35.37456436289383]
prefecture: Kanagawa
area: Tanzawa
tags: [.draft]
summary: >
  This course is marked by several waterfalls in the first half of the course, followed by a 45 minute relatively steep uphill climb and then a more relaxing up-down course through beautiful cedar forests.
---


## Points of interest

 - Waterfalls (黒龍の滝 and 水日の滝)
 - Beautiful Cedar Forests
 - Tea Plantations with nice views near the end

## Access

Shibusawa Station (渋沢駅) is on the Odakyuu Line (小田急線) about 1 and a half hours from Shinjuku

## Course

From Shibusawa Station, go out the North Exit to bus stop # 2 and take the bus going to Ookura (大倉). Take the bus to the end (about 20 minutes). Go across the main street there to a small street that has a sign pointing to Niho (仁保)and walk up this road. Watch for the Niho signs and they will take you to the hiking course. It takes about 1.5 hours to go there (and it’s on a creek where you can have lunch). On the way there, there a some side paths to the different waterfalls which are well worth going to, and may set you back about 30 minutes if you do them. From Niho, take the hiking path which goes to the northwest going towards Mt. Nabewari (鍋割山), don’t go towards Tonodake (塔ノ岳). After walking up a relatively steep incline, you’ll come to a junction. To the right is Mt. Nabewari and to the left is Kurinokidou (栗の木洞).

Go towards Kurinokido, with several ups and downs for about 30 minutes. After that, it’s mainly a downhill journey past Mt Kunugi (櫟山)and down through some tea fields with nice views, finishing off at Uzumo Bus Stop. This bus takes you to ShinMatsuda Station, also on the Odakyuu Line. Be careful to check the bus schedule beforehand since the buses are only about every 1.5 - 2 hours. Walking time - about 5 hours at a normal pace, for relatively fit people.

{% flickr http://www.flickr.com/photos/jambodave/11741956284 %}
{% flickr http://www.flickr.com/photos/jambodave/11741561235 %}
{% flickr http://www.flickr.com/photos/jambodave/11741566085 %}
{% flickr http://www.flickr.com/photos/jambodave/11742320986 %}
{% flickr http://www.flickr.com/photos/jambodave/11741786363 %}
