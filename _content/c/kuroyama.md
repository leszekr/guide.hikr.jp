---
layout: course
title: Kuroyama waterfalls
center: [ 139.2451000213623, 35.94076799566985 ]
map:
 - /data/c/kuroyama.json
 - /data/area/Chichibu.json
author: hikr.jp
license: CC-BY-NC
area: Chichibu
prefecture: Saitama
header_image: http://www.flickr.com/photos/jambodave/11040041165/
header_class: vertical
info:
  level: intermediate
tags: [ waterfall, .draft ]
summary: >
  Kuroyama Santaki was once a training center for monks who practice ascetism, but now it has become a sightseeing spot for tourists.
 
  The waterfals are just one stop in this moderate 6-hour walking course.
---

## Points of interest

 - Kaburi pass
 - Kasasugi pass
 - Kuroyama santaki

## Access

The quickest way to get to Agano station (吾野駅) from Tokyo is via the Seibu Ikebukuro express towards Nagatoro　(長瀞).

## Course

From Agano station’s north exit, walk east on the road until you cross a bridge. From there, you want to head north, following signs to Kaburi pass (顔振峠).


{% flickr http://www.flickr.com/photos/jambodave/11040192573/ large half %}

{% flickr http://www.flickr.com/photos/jambodave/11040150264/ large half %}

Shortly before Kaburi pass, there's a wonderful view of a rural area and Mt. Fuji in the distance, if weather permits. Kaburi pass hosts some restaurants, and there's a big map and a slew of signs. Follow the sign to Kasasugi pass (傘杉峠), not the path that goes up the mountain (on the other side of the road). This sign at this point is a bit unclear. Simply follow the asphalt road for about 20 minutes, then take a small path on your right with a sign pointing to Kasasugi pass (傘杉峠) for another 20 minutes. You will arrive at the pass, where you will find a convergence of paths.

{% flickr http://www.flickr.com/photos/jambodave/11040117836/ %}

There’s a sign on your right pointing to Kuroyama Santaki (黒山三滝). Take this path and, in about 40 minutes, you’ll arrive at a picnic table with 2 nice waterfalls nearby. Great place to eat. The waterfalls used to be a training center for monks who practise ascetism. You'll meet groups of tourists, some of whom came by car. You may enjoy grilled fish-on-a-stick or stay in a hotel or relax in Kurotaki Onsen here.

{% flickr http://www.flickr.com/photos/jambodave/11040032065/ large half %}

{% flickr http://www.flickr.com/photos/jambodave/11040027445/ large half %}

From the waterfall area, walk down the paved road until you reach the city. There's a bus stop there, with busses leaving once per hour to Ogose station. Walk along the road there, look to your right to for the place where the hiking course starts again.

{% flickr http://www.flickr.com/photos/jambodave/11040179074/ large half %}

{% flickr http://www.flickr.com/photos/jambodave/11040048085/ large half %}

At some point the mountain path ends in an orchard area which turns into a residential area. You'll see a toilet on your left and on your right is a staircase leading up to a shrine. There's a giant bell and a sign saying, basically "Enjoy!" (楽しんで下さい). At the shrine, turn right and continue on the hiking course to Ogose station (越生駅).


{% flickr http://www.flickr.com/photos/jambodave/11040200014/ large half %}

{% flickr http://www.flickr.com/photos/jambodave/11040246013/ large half %}

Take this course in November for wonderful views of red leaves.