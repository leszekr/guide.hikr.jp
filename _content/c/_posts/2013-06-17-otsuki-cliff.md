---
layout: course
title: Otsuki cliff
center: [138.94816875457764, 35.620639996677504]
map: 
 - /data/c/otsuki-cliff.json
author: hikr.jp
license: CC-BY-NC
tags: [ chains, cliff ]
area: Otsuki
prefecture: Yamanashi
summary: >
  The cliffs near Iwadono mountain were, legend has it, an excelent location for a castle, making it hard to access for potential attackers. Currently, the cliffs are an attraction for hikers, looking for an exciting course. Not recommended if you have a fear of heights, or can't pull your weight up a chain.
---

## Access

From Shinjuku, take the Chuo line to Otsuki station (大月駅).

## Course

From Otsuki station, head east, keeping your eyes on Mt. Iwadono (岩殿山). It's the one with the barren rocky sides you could see from the train. Cross the river and you'll get to the highway. The hiking path starts on the other side of the highway and turns north-west sharply.

The first part of the path is mostly climbing stairs. There are some buildings including a motel, tourist information center and some religious facilities. You could climb to the top of Iwadono-san, but continue on the path to the cliff.

{% flickr http://www.flickr.com/photos/jambodave/7386274274/ %}

The path is mostly straightforward from there, but requires extra care at times, where you'll be walking along a narrow rocky edge and clibming up using chains. Best bring gloves to make your hands less slippery.

{% flickr http://www.flickr.com/photos/jambodave/7386278772/ %}

You'll see the main attraction of the course before you get there: the main cliff is visible from the path. Legends say, the residents of the castle (of which now not much remains), threw their children off this cliff, hoping that the long fall would be a better fate for them than the attackers.

Past the cliff, head on the path back to the residential area and follow the river back to Otsuki station.