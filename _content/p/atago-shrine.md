---
layout: place
type: shrine
label:
  jp: 愛宕神社
  en: Atago Shrine
location: [ 139.09729957580566, 35.80422287822467]
area: Okutama
tags: [ shrine ]
prefecture: Tokyo
---