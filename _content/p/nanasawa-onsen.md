---
layout: place
type: onsen
label:
  jp: 七沢温泉
  en: Nanasawa onsen
location: [139.27720069885254, 35.45455256831708]
area: Tanzawa
prefecture: Kanagawa
tags: [ onsen ]
---
