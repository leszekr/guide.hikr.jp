---
layout: place
type: onsen
label:
  en: Kuroyama Onsen
  jp: 黒山温泉
location: [139.25346851348877, 35.93906545754889]
area: Chichibu
prefecture: Saitama
tags: [ onsen ]
---
