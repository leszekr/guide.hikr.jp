---
type: shrine
layout: place
location: [ 139.15008544921875, 35.78290181265832]
area: Okutama
prefecture: Tokyo
tags: [ shrine ]
label:
  jp: 御嶽神社
  en: Mitake shrine
---