---
type: campsite
layout: place
location: [ 139.0995740890503, 35.80690283592832 ]
label:
  jp: 氷川キャンプ場
  en: Hikawa capsite
area: Okutama
tags: [ camp ]
prefecture: Tokyo
---