---
layout: place
type: temple
tags: [ temple, statue ]
label: 
  en: Jinmuji temple
  jp: 神武寺
location: [139.60792779922485, 35.30484667292013]
area: Miura
prefecture: Kanagawa
---