# TO DO

## Pre-release

### Content creation

 - [ ] footer content
 - [v] make place pages for mountains

### Checking

 - [ ] check all pages, maybe hide some
 - [ ] check all mountain elevations
 - [ ] check on IE

### search

 - [ ] multi keyword search (best matches first)
 - [ ] search results page
 - [ ] style search results
 - [ ] search results keyboard select
 - [ ] add tags to search

### Styling

 - [ ] make map show on small screens

-------------------------------------------------------

## Post-release

 - [ ] weather display
 - [ ] elevation plot
 - [ ] train lines as data files