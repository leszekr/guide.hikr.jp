require "rack/jekyll"
require "rack/rewrite"
require './app.rb'

run Rack::URLMap.new("/" => Rack::Jekyll.new,
                     "/api" => Hikr::API
                     )
